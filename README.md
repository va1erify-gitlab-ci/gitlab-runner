# Gitlab CI для установки и регистрации Gitlab runner

## Переменные

- `VARIABLES_ANSIBLE_REPOSITORY`: Репозиторий с Ansible ролью для установки GitLab Runner.
- `VARIABLES_BRANCH`: Ветка или тег в репозитории с Ansible ролью.
- `VARIABLES_GITLAB_RUNNER_TOKEN`: Токен для регистрации GitLab Runner.
- `VARIABLES_HOST_IP`: IP-адрес хоста, на котором будет установлен Runner.
- `VARIABLES_HOST_USER`: Имя пользователя для подключения к хосту.
- `VARIABLES_IMAGE`: Образ с установленным Ansible.
- `VARIABLES_PRIVATE_KEY`: Приватный ключ для доступа к виртуальной машине (ВМ).
- `VARIABLES_ROLENAME`: Название для директории с Ansible ролью.

# Используемые репозитории
- [Ansible роль для установки и регистрации gitlab runner](https://gitlab.com/va1erify-ansible-roles/gitlab-runner)